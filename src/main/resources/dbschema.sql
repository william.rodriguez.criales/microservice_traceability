DROP DATABASE IF EXISTS `traceability`;
CREATE DATABASE  IF NOT EXISTS `traceability`;

--
-- Table traceability
--

DROP TABLE IF EXISTS `traceability`.`traceability`;
CREATE TABLE `traceability`.`traceability` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_order` bigint(20) NOT NULL,
  `id_client` bigint(20) NOT NULL,
  `email_client` varchar(45) NOT NULL,
  `date` date NOT NULL,
  `status_back` varchar(45) NOT NULL,
  `status_new` varchar(45) NOT NULL,
  `id_employee` bigint(45) NOT NULL,
  `email_employee` varchar(45) NOT NULL,

  PRIMARY KEY (`id`)
);




