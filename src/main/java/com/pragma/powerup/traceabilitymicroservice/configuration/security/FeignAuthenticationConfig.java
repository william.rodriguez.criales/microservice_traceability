package com.pragma.powerup.traceabilitymicroservice.configuration.security;

import com.pragma.powerup.traceabilitymicroservice.configuration.security.jwt.JwtUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;

public class FeignAuthenticationConfig {

    @Bean
    public RequestInterceptor bearerTokenRequestInterceptor() {
        return new RequestInterceptor() {


            @Override
            public void apply(RequestTemplate template) {
                template.header("Authorization",
                        String.format("Bearer %s", JwtUtils.getStringToken()));
            }
        };
    }


}
