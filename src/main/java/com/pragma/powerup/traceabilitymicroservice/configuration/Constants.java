package com.pragma.powerup.traceabilitymicroservice.configuration;

public class Constants {

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    public static final Long ADMIN_ROLE_ID = 1L;
    public static final Long EMPLOYEE_ROLE_ID = 2L;
    public static final Long CLIENT_ROLE_ID = 3L;
    public static final Long OWNER_ROLE_ID = 4L;

    public static final String RESPONSE_MESSAGE_KEY = "message";
    public static final String TWILIO_ACCOUNT_SID = "AC632636391d908063f3ec6558ba6eccd8";
    public static final String TWILIO_AUTH_TOKEN = "0bd160030b30a1456562990acd074429";
    public static final String RESTAURANT_CREATED_MESSAGE = "Restaurant created successfully";
    public static final String RESPONSE_ERROR_MESSAGE_KEY = "error";
    public static final String ERROR_MESSAGE = "Error calling external api";
    public static final String WRONG_CREDENTIALS_MESSAGE = "Unauthorized, Provide a valid token";
    public static final String ACCESS_DENIED_MESSAGE = "Access denied, the user does not have the right permissions";
    public static final String NO_DATA_FOUND_MESSAGE = "No data found for the requested petition";
    public static final String RESTAURANT_NOT_FOUND_MESSAGE = "restaurant does not exist";
    public static final String DISH_NOT_FOUND_MESSAGE = "dish does not exist";
    public static final String ROLE_NOT_ALLOWED_MESSAGE = "No permission granted to create users with this role";
    public static final String SWAGGER_TITLE_MESSAGE = "Traceability API Pragma Power Up";
    public static final String SWAGGER_DESCRIPTION_MESSAGE = "Traceability microservice";
    public static final String SWAGGER_VERSION_MESSAGE = "1.0.0";
    public static final String SWAGGER_LICENSE_NAME_MESSAGE = "Apache 2.0";
    public static final String SWAGGER_LICENSE_URL_MESSAGE = "http://springdoc.org";
    public static final String SWAGGER_TERMS_OF_SERVICE_MESSAGE = "http://swagger.io/terms/";
    public static final String WRONG_VALUES_MESSAGE = "Wrong values";

}
