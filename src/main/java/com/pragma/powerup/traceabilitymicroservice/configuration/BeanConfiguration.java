package com.pragma.powerup.traceabilitymicroservice.configuration;

import com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.adapter.TraceabilityMysqlAdapter;
import com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.mappers.ITraceabilityEntityMapper;
import com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.repositories.ITraceabilityRepository;
import com.pragma.powerup.traceabilitymicroservice.domain.api.ITraceabilityServicePort;
import com.pragma.powerup.traceabilitymicroservice.domain.spi.ITraceabilityPersistencePort;
import com.pragma.powerup.traceabilitymicroservice.domain.usecase.TraceabilityUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final ITraceabilityRepository traceabilityRepository;
    private final ITraceabilityEntityMapper traceabilityEntityMapper;

    @Bean
    public ITraceabilityPersistencePort traceabilityPersistencePort() {
        return new TraceabilityMysqlAdapter(traceabilityRepository, traceabilityEntityMapper);
    }
    @Bean
    public ITraceabilityServicePort traceabilityServicePort() {
        return new TraceabilityUseCase(traceabilityPersistencePort());
    }



}
