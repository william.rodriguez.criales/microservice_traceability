package com.pragma.powerup.traceabilitymicroservice.configuration.security.jwt;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.Optional;

public class JwtUtils {
    private static Optional<Jwt> getToken() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(JwtAuthenticationToken.class::cast)
                .map(JwtAuthenticationToken::getToken);

    }

    public static String getStringToken(){
        return getToken().map(Jwt::getTokenValue).orElse("");
    }

    public  static String getClaim(String stringClaim){
        return getToken().map(token-> token.getClaimAsString(stringClaim)).orElse("");
    }
}
