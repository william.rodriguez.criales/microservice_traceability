package com.pragma.powerup.traceabilitymicroservice.domain.spi;


import com.pragma.powerup.traceabilitymicroservice.domain.model.Traceability;
import lombok.SneakyThrows;

import java.util.List;

public interface ITraceabilityPersistencePort {

    @SneakyThrows
    List<Traceability> getAllOrdersClient(Long idClient);

    Traceability saveTraceability(Traceability traceability);

    List<Traceability> getAllOrdersStatusDeliver();
}
