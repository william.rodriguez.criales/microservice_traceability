package com.pragma.powerup.traceabilitymicroservice.domain.model;

public class OrderEfficiency {
    private Long idOrder;
    private Long time;

    public OrderEfficiency(Long idOrder, Long time) {
        this.idOrder = idOrder;
        this.time = time;
    }

    public Long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
