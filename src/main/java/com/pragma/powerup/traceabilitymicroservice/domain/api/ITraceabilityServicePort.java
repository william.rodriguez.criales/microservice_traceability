package com.pragma.powerup.traceabilitymicroservice.domain.api;

import com.pragma.powerup.traceabilitymicroservice.domain.model.OrderEfficiency;
import com.pragma.powerup.traceabilitymicroservice.domain.model.Traceability;

import java.util.List;

public interface ITraceabilityServicePort {
    List<Traceability> getAllOrdersClient();

    Traceability saveTraceability(Traceability traceability);

    List<OrderEfficiency> getAllOrdersEfficiency();
}
