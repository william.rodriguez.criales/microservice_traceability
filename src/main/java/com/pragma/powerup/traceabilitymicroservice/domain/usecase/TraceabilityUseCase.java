package com.pragma.powerup.traceabilitymicroservice.domain.usecase;

import com.pragma.powerup.traceabilitymicroservice.configuration.security.jwt.JwtUtils;
import com.pragma.powerup.traceabilitymicroservice.domain.api.ITraceabilityServicePort;
import com.pragma.powerup.traceabilitymicroservice.domain.model.OrderEfficiency;
import com.pragma.powerup.traceabilitymicroservice.domain.model.Traceability;
import com.pragma.powerup.traceabilitymicroservice.domain.spi.ITraceabilityPersistencePort;
import org.springframework.util.ObjectUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class TraceabilityUseCase implements ITraceabilityServicePort {

    private final ITraceabilityPersistencePort traceabilityPersistencePort;

    public TraceabilityUseCase(ITraceabilityPersistencePort traceabilityPersistencePort) {
        this.traceabilityPersistencePort = traceabilityPersistencePort;
    }


    @Override
    public List<Traceability> getAllOrdersClient() {


        Long idClient = Long.parseLong(JwtUtils.getClaim("idUser"));

        return traceabilityPersistencePort.getAllOrdersClient(idClient);
    }

    //@Override
    public List<OrderEfficiency> getAllOrdersEfficiency() {

        List<Traceability> traceabilityList = traceabilityPersistencePort.getAllOrdersStatusDeliver();


        Map<Long, List<Traceability>> ordersGroupById = traceabilityList.stream().collect(Collectors.groupingBy(Traceability::getIdOrder));
        List<OrderEfficiency> orderEfficiencyList = new ArrayList<>();
        for (Long key : ordersGroupById.keySet()) {


            List<Traceability> traceabilityStatuses = ordersGroupById.get(key);
            Traceability traceabilityDelivery = new Traceability();
            Traceability traceabilityPending = new Traceability();
            if (traceabilityStatuses.size() > 1) {
                for (Traceability traceability : traceabilityStatuses) {

                    if (traceability.getStatusNew().equals("entregado")) {
                        traceabilityDelivery = traceability;
                    }
                    if (traceability.getStatusNew().equals("pendiente")) {
                        traceabilityPending = traceability;
                    }

                }

                Duration res = Duration.between(traceabilityPending.getDate(), traceabilityDelivery.getDate());
                OrderEfficiency orderEfficiency = new OrderEfficiency(traceabilityDelivery.getIdOrder(), res.toMinutes());
                orderEfficiencyList.add(orderEfficiency);
            }


        }
        return orderEfficiencyList;

    }

    @Override
    public Traceability saveTraceability(Traceability traceability) {
        traceability.getDate();
        return traceabilityPersistencePort.saveTraceability(traceability);
    }


    //validacion phone: numeros menores que 13 digitos y mayores a 7 digitos, debe permiter el +
    private boolean isPhoneValid(String phone) {
        return phone.matches("^[+]?(\\d){7,12}$");

    }


    private boolean isEmpty(Object field) {
        //return field.matches("^[''' ']*$") || field==null ; opcion 1
        return ObjectUtils.isEmpty(field);
    }

    private boolean isNameValid(String name) {
        return name.matches("^[0-9a-zA-Z]*$") && !(name.matches("^[\\d]*$"));
    }

}
