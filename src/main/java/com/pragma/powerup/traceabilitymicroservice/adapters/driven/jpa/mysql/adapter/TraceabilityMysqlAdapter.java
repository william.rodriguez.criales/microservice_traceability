package com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.entity.TraceabilityEntity;
import com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.mappers.ITraceabilityEntityMapper;
import com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.repositories.ITraceabilityRepository;
import com.pragma.powerup.traceabilitymicroservice.domain.model.Traceability;
import com.pragma.powerup.traceabilitymicroservice.domain.spi.ITraceabilityPersistencePort;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.util.List;

@RequiredArgsConstructor
public class TraceabilityMysqlAdapter implements ITraceabilityPersistencePort {
    private final ITraceabilityRepository traceabilityRepository;
    private final ITraceabilityEntityMapper traceabilityEntityMapper;


    @SneakyThrows
    @Override
    public List<Traceability> getAllOrdersClient(Long idClient) {

        List<TraceabilityEntity> traceabilityEntityList = traceabilityRepository.findAllByIdClient(idClient);

        return traceabilityEntityList.stream().map(traceabilityEntityMapper::toTraceability).toList();
    }

    @Override
    public Traceability saveTraceability(Traceability traceability){

        TraceabilityEntity traceabilityEntity = traceabilityRepository.saveAndFlush(traceabilityEntityMapper.toEntity(traceability)) ;
        return traceabilityEntityMapper.toTraceability(traceabilityEntity);
    }

    @Override
    public List<Traceability> getAllOrdersStatusDeliver() {

        List<TraceabilityEntity> traceabilityEntityList = traceabilityRepository.findAllByStatusNewIn(List.of("pendiente","entregado"));

        return traceabilityEntityList.stream().map(traceabilityEntityMapper::toTraceability).toList();
    }
}
