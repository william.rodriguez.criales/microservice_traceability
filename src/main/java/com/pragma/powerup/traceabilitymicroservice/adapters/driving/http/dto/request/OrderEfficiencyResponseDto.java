package com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OrderEfficiencyResponseDto {

    private Long idOrder;
    private Long time;
}
