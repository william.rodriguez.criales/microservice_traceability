package com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.controller;


import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request.OrderEfficiencyResponseDto;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request.TraceabilityRequestDto;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.response.TraceabilityResponseDto;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.handlers.ITraceabilityHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/traceability")
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
public class TraceabilityRestController {
    private final ITraceabilityHandler traceabilityHandler;

    @Operation(summary = "Add a traceability",
            responses = {
                    @ApiResponse(responseCode = "201", description = "traceability created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "user already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})

    @PostMapping
    public ResponseEntity<TraceabilityResponseDto> saveTraceability(@RequestBody TraceabilityRequestDto traceabilityRequestDto) {
        TraceabilityResponseDto traceabilityResponseDto = traceabilityHandler.saveTraceability(traceabilityRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(traceabilityResponseDto);
    }

    @Operation(summary = "consult traceability",
            responses = {
                    @ApiResponse(responseCode = "201", description = "ok traceability",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map")))
            })

    @GetMapping("/consult")
    public ResponseEntity<List<TraceabilityResponseDto>> getById() {
        List<TraceabilityResponseDto> traceabilityResponseDto = traceabilityHandler.getAllOrdersClient();
        return ResponseEntity.status(HttpStatus.OK).body(traceabilityResponseDto);

    }

    @Operation(summary = "efficiency traceability",
            responses = {
                    @ApiResponse(responseCode = "201", description = "ok traceability",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map")))
            })

    @GetMapping("/efficiency")
    public ResponseEntity<List<OrderEfficiencyResponseDto>> getAllOrdersEfficiency() {
        List<OrderEfficiencyResponseDto> OrderEfficiencyResponseDto = traceabilityHandler.getAllOrdersEfficiency();
        return ResponseEntity.status(HttpStatus.OK).body(OrderEfficiencyResponseDto);

    }
}



