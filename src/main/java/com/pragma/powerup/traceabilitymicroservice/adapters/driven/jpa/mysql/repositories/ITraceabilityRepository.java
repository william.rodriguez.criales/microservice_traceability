package com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.repositories;


import com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.entity.TraceabilityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ITraceabilityRepository extends JpaRepository<TraceabilityEntity, Long> {

    List<TraceabilityEntity> findAllByIdClient(Long idClient);

    List<TraceabilityEntity> findAllByStatusNewIn(List<String> pendiente);
}
