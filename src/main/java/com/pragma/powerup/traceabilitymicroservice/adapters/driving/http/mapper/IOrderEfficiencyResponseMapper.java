package com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request.OrderEfficiencyResponseDto;
import com.pragma.powerup.traceabilitymicroservice.domain.model.OrderEfficiency;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IOrderEfficiencyResponseMapper {

    OrderEfficiencyResponseDto toOrderEfficiencyResponseDto(OrderEfficiency orderEfficiency);

}


