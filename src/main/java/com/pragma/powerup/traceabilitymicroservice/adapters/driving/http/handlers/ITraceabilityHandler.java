package com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request.OrderEfficiencyResponseDto;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request.TraceabilityRequestDto;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.response.TraceabilityResponseDto;

import java.util.List;

public interface ITraceabilityHandler {
    List<TraceabilityResponseDto> getAllOrdersClient();

    TraceabilityResponseDto saveTraceability(TraceabilityRequestDto traceabilityRequestDto);

    List<OrderEfficiencyResponseDto> getAllOrdersEfficiency();
}
