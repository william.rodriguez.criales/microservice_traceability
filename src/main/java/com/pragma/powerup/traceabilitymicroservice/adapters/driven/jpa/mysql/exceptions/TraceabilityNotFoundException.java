package com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.exceptions;

public class TraceabilityNotFoundException extends Throwable {

    public TraceabilityNotFoundException() {
        super();
    }
}
