package com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.handlers.impl;


import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request.OrderEfficiencyResponseDto;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request.TraceabilityRequestDto;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.response.TraceabilityResponseDto;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.mapper.IOrderEfficiencyResponseMapper;
import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.mapper.ITraceabilityRequestMapper;

import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.handlers.ITraceabilityHandler;

import com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.mapper.ITraceabilityResponseMapper;

import com.pragma.powerup.traceabilitymicroservice.domain.api.ITraceabilityServicePort;

import com.pragma.powerup.traceabilitymicroservice.domain.model.OrderEfficiency;
import com.pragma.powerup.traceabilitymicroservice.domain.model.Traceability;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TraceabilityHandlerImpl implements ITraceabilityHandler {
    private final ITraceabilityServicePort traceabilityServicePort;
    private final ITraceabilityRequestMapper traceabilityRequestMapper;
    private final ITraceabilityResponseMapper traceabilityResponseMapper;
    private final IOrderEfficiencyResponseMapper orderEfficiencyResponseMapper;

    @Override
    public List<TraceabilityResponseDto> getAllOrdersClient() {
        List<Traceability> traceability = traceabilityServicePort.getAllOrdersClient();
        return traceability.stream().map(traceabilityResponseMapper::toTraceabilityResponseDto).toList();
    }

    @Override
    public TraceabilityResponseDto saveTraceability(TraceabilityRequestDto traceabilityRequestDto) {
        Traceability traceability =traceabilityRequestMapper.toTraceability(traceabilityRequestDto);
        traceability = traceabilityServicePort.saveTraceability(traceability);
        return traceabilityResponseMapper.toTraceabilityResponseDto(traceability);
    }

    @Override
    public List<OrderEfficiencyResponseDto> getAllOrdersEfficiency () {
        List<OrderEfficiency> OrderEfficiencyList = traceabilityServicePort.getAllOrdersEfficiency();
        return OrderEfficiencyList.stream().map(orderEfficiencyResponseMapper::toOrderEfficiencyResponseDto).toList();
    }


}
