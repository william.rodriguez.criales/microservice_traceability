package com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Table(name = "traceability")
@NoArgsConstructor
@AllArgsConstructor
//@Getter
//@Setter
@Data
public class TraceabilityEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long idOrder;
    private Long idClient;
    private String emailClient;
    private Date date;
    private String statusBack;
    private String statusNew;
    private Long idEmployee;
    private String emailEmployee;


}
