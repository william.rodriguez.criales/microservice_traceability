package com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.mappers;


import com.pragma.powerup.traceabilitymicroservice.adapters.driven.jpa.mysql.entity.TraceabilityEntity;
import com.pragma.powerup.traceabilitymicroservice.domain.model.Traceability;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface ITraceabilityEntityMapper {
    TraceabilityEntity toEntity(Traceability traceability);

    Traceability toTraceability(TraceabilityEntity traceabilityEntity);
}
