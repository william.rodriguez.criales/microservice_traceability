package com.pragma.powerup.traceabilitymicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
public class TraceabilityRequestDto {
    private Long idOrder;
    private Long idClient;
    private String emailClient;
    private LocalDateTime date;
    private String statusBack;
    private String statusNew;
    private Long idEmployee;
    private String emailEmployee;

}
