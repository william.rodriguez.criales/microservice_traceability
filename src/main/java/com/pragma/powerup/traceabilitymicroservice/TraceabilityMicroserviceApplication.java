package com.pragma.powerup.traceabilitymicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;



@SpringBootApplication(exclude= SecurityAutoConfiguration.class)

public class TraceabilityMicroserviceApplication {

	public static void main(String[] args) {

		SpringApplication.run(TraceabilityMicroserviceApplication.class, args);
	}

}

